using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FaceTracking
{
	[CreateAssetMenu]
	public class AimAtPlayer : ShootAtPlayer
	{
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			spawner.up = GetShootDirection(spawner);
			return new Bullet[0];
		}
	}
}