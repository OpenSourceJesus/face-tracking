﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	[CreateAssetMenu]
	public class RepeatBulletPatterns : BulletPattern
	{
		[MakeConfigurable]
		public int repeatCount;
		public BulletPattern[] bulletPatterns;

		public override void Init (Transform spawner)
		{
			base.Init (spawner);
			foreach (BulletPattern bulletPattern in bulletPatterns)
				bulletPattern.Init (spawner);
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			List<Bullet> output = new List<Bullet>();
			for (int i = 0; i < repeatCount; i ++)
			{
				foreach (BulletPattern bulletPattern in bulletPatterns)
					output.AddRange(bulletPattern.Shoot (spawner, bulletPrefab, positionOffset));
			}
			return output.ToArray();
		}
		
		public override Bullet[] Shoot (Vector2 spawnPos, Vector2 direction, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = new Bullet[0];
			for (int i = 0; i < repeatCount; i ++)
			{
				foreach (BulletPattern bulletPattern in bulletPatterns)
					output = output.AddRange(bulletPattern.Shoot (spawnPos, direction, bulletPrefab, positionOffset));
			}
			return output;
		}
	}
}