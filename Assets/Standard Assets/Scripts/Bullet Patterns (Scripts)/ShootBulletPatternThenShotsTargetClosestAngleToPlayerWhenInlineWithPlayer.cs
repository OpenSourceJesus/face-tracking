using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenShotsTargetClosestAngleToPlayerWhenInlineWithPlayer : BulletPattern
	{
		public BulletPattern bulletPattern;
		public float[] retargetAngles;
		[HideInInspector]
		[SerializeField]
		Vector2[] retargetDirections = new Vector2[0];
		public float lineWidth;
		public bool lastRetarget;

		public override void Init (Transform spawner)
		{
			retargetDirections = new Vector2[retargetAngles.Length];
			for (int i = 0; i < retargetAngles.Length; i ++)
				retargetDirections[i] = VectorExtensions.FromFacingAngle(retargetAngles[i]);
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = bulletPattern.Shoot (spawner, bulletPrefab, positionOffset);
			GameManager.GetSingleton<GameManager>().StartCoroutine(RetargetShotsWhenInlineWithPlayerRoutine (output));
			return output;
		}

        public virtual IEnumerator RetargetShotsWhenInlineWithPlayerRoutine (Bullet[] bullets)
		{
			Bullet bullet;
			Vector2 retargetDirection;
			do
			{
				for (int i = 0; i < bullets.Length; i ++)
				{
					bullet = bullets[i];
					if (bullet.timesRetargeted == 0 && bullet.gameObject.activeSelf)
					{
						for (int i2 = 0; i2 < retargetDirections.Length; i2 ++)
						{
							retargetDirection = retargetDirections[i2];
							if (Physics2DExtensions.LinecastWithWidth(bullet.trs.position, (Vector2) bullet.trs.position + retargetDirection * Vector2.Distance(GameManager.GetSingleton<Player>().trs.position, bullet.trs.position), lineWidth, LayerMask.GetMask("Player")))
							{
								bullet.Retarget (retargetDirection, lastRetarget);
								break;
							}
						}
					}
					else
					{
						bullets = bullets.Remove(bullet);
						i --;
						if (bullets.Length == 0)
							yield break;
					}
				}
				yield return new WaitForEndOfFrame();
			} while (true);
		}
	}
}