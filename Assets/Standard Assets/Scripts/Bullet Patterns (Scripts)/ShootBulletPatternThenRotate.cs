﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenRotate : BulletPattern
	{
		public BulletPattern bulletPattern;
		[MakeConfigurable]
		public float rotate;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = bulletPattern.Shoot (spawner, bulletPrefab, positionOffset);
			spawner.up = spawner.up.Rotate(rotate);
			return output;
		}
	}
}