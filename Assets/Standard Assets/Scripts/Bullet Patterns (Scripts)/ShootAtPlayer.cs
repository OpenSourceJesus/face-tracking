﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FaceTracking
{
	[CreateAssetMenu]
	public class ShootAtPlayer : BulletPattern
	{
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return GameManager.GetSingleton<Player>().trs.position - spawner.position;
		}
	}
}