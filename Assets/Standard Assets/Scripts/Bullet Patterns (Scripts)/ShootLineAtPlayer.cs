using System.Collections;
using System.Collections.Generic;
using UnityEngine;
	
namespace FaceTracking
{
	[CreateAssetMenu]
	public class ShootLineAtPlayer : ShootWhereFacing
	{
		public int bulletCount;
		public float lineLength;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			spawner.up = GameManager.GetSingleton<Player>().trs.position - spawner.position;
			Bullet[] output = new Bullet[bulletCount];
			for (int i = 0; i < bulletCount; i ++)
				output[i] = GameManager.GetSingleton<ObjectPool>().SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position, spawner.rotation);
			GameManager.GetSingleton<GameManager>().StartCoroutine(TravelRoutine (spawner, output));
			return output;
		}

		public virtual IEnumerator TravelRoutine (Transform spawner, Bullet[] bullets)
		{
			Vector2 bulletsStartPosition = spawner.position;
			List<BulletInLine> bulletsInLine = new List<BulletInLine>();
			LineSegment2D line = new LineSegment2D();
			line.start = spawner.position - (spawner.right * lineLength / 2);
			line.end = spawner.position + (spawner.right * lineLength / 2);
			Bullet bullet;
			float directedDistAlongLine;
			BulletInLine bulletInLine;
			for (int i = 0; i < bullets.Length; i ++)
			{
				bullet = bullets[i];
				if (bulletCount % 2 != 0)
					directedDistAlongLine = lineLength * ((float) i / lineLength);
				else
					directedDistAlongLine = lineLength * (((float) i + 0.5f) / lineLength);
				bulletInLine = new BulletInLine(bullet, line.GetPointWithDirectedDistance(directedDistAlongLine));
				bulletsInLine.Add(bulletInLine);
				bullet.Retarget (bulletInLine.destination - (Vector2) bullet.trs.position);
			}
			do
			{
				for (int i = 0; i < bulletsInLine.Count; i ++)
				{
					bulletInLine = bulletsInLine[i];
					if ((bulletsStartPosition - (Vector2) bulletInLine.bullet.trs.position).sqrMagnitude >= (bulletInLine.destination - bulletsStartPosition).sqrMagnitude)
					{
						bulletInLine.bullet.rigid.velocity = Vector2.zero;
						bulletInLine.bullet.trs.position = bulletInLine.destination;
						bulletInLine.bullet.trs.up = spawner.up;
						bulletsInLine.RemoveAt(i);
						i --;
					}
				}
				yield return new WaitForEndOfFrame();
			} while (bulletsInLine.Count > 0);
			for (int i = 0; i < bullets.Length; i ++)
				bullets[i].Retarget (spawner.up, true);
		}

		public class BulletInLine
		{
			public Bullet bullet;
			public Vector2 destination;

			public BulletInLine (Bullet bullet, Vector2 destination)
			{
				this.bullet = bullet;
				this.destination = destination;
			}
		}
	}
}