using UnityEngine;
using Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FaceTracking
{
	//[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CompositeCollider2D))]
	[RequireComponent(typeof(SpriteRenderer))]
	public class EnemyBattle : MonoBehaviour
	{
		public Enemy[] enemies = new Enemy[0];
		public AwakableEnemy[] awakableEnemies = new AwakableEnemy[0];
		public Transform trs;
		public CompositeCollider2D compositeCollider;
		public GameObject followRangesVisualizerGo;
#if UNITY_EDITOR
		public MakeCirclePolygonCollider2D[] makeCirclePolygonColliders;
		public bool update;
		public int pointsPerPolygonCollider;
#endif
		int enemiesDead;
		// public int money;
		// public bool HasBeenDefeated
		// {
		// 	get
		// 	{
		// 		return SaveAndLoadManager.GetNonSharedValue<bool>(name + " defeated", false);
		// 	}
		// 	set
		// 	{
		// 		SaveAndLoadManager.SetNonSharedValue(name + " defeated", value);
		// 	}
		// }

		public virtual void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (compositeCollider == null)
					compositeCollider = GetComponent<CompositeCollider2D>();
				makeCirclePolygonColliders = GetComponents<MakeCirclePolygonCollider2D>();
				if (GameManager.GetSingleton<GameManager>().doEditorUpdates)
					EditorApplication.update += DoEditorUpdate;
				// List<RemovedComponent> removedComponents = PrefabUtility.GetRemovedComponents(gameObject);
				// foreach (RemovedComponent removedComponent in removedComponents)
				// 	removedComponent.Revert();
				// MakeSpriteFromCollider2D makeSpriteFromCollider = GetComponentInChildren<MakeSpriteFromCollider2D>();
				// if (makeSpriteFromCollider != null && makeSpriteFromCollider.enabled && makeSpriteFromCollider.spriteRenderer.sprite == null)
				// 	makeSpriteFromCollider.Do ();
				return;
			}
			else
				EditorApplication.update -= DoEditorUpdate;
#endif
			foreach (Enemy enemy in enemies)
				enemy.SetOnDeathListener(OnEnemyDeath);
		}

		public virtual void OnEnemyDeath ()
		{
			enemiesDead ++;
			if (enemiesDead == enemies.Length)
			{
				// if (!HasBeenDefeated)
				// {
				// 	GameManager.GetSingleton<Player>().AddMoney (money);
				// 	HasBeenDefeated = true;
				// }
				followRangesVisualizerGo.SetActive(false);
			}
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			WorldMapIcon worldMapIcon = other.GetComponent<WorldMapIcon>();
			if (worldMapIcon != null && worldMapIcon.isActive)
				return;
			if (enemiesDead < enemies.Length)
			{
				foreach (AwakableEnemy awakableEnemy in awakableEnemies)
				{
					awakableEnemy.invulnerable = false;
					if (awakableEnemy.spriteSkin != null)
						awakableEnemy.spriteSkin.enabled = true;
				}
			}
			// followRangesVisualizerGo.SetActive(true);
		}

		public virtual void OnTriggerExit2D (Collider2D other)
		{
			WorldMapIcon worldMapIcon = other.GetComponent<WorldMapIcon>();
			if (worldMapIcon != null && worldMapIcon.isActive)
				return;
			if (enemiesDead < enemies.Length)
			{
				enemiesDead = 0;
				foreach (AwakableEnemy awakableEnemy in awakableEnemies)
					awakableEnemy.Reset ();
			}
			followRangesVisualizerGo.SetActive(false);
		}

#if UNITY_EDITOR
		AwakableEnemy awakableEnemy;
		MakeCirclePolygonCollider2D makeCirclePolygonCollider;
		Transform followRangeTrs;
		public virtual void DoEditorUpdate ()
		{
			if (!update)
				return;
			update = false;
			PolygonCollider2D polygonCollider;
			foreach (MakeCirclePolygonCollider2D makeCirclePolygonCollider in makeCirclePolygonColliders)
			{
				polygonCollider = makeCirclePolygonCollider.polygonCollider;
				DestroyImmediate(makeCirclePolygonCollider);
				DestroyImmediate(polygonCollider);
			}
			makeCirclePolygonColliders = new MakeCirclePolygonCollider2D[0];
			foreach (AwakableEnemy awakableEnemy in awakableEnemies)
			{
				makeCirclePolygonCollider = gameObject.AddComponent<MakeCirclePolygonCollider2D>();
				if (awakableEnemy.followRangeCollider == null)
					awakableEnemy.followRangeCollider = awakableEnemy.GetComponent<Transform>().Find("Follow Range").GetComponent<CircleCollider2D>();
				followRangeTrs = awakableEnemy.followRangeCollider.GetComponent<Transform>();
				makeCirclePolygonCollider.circle.center = (Vector2) followRangeTrs.position + awakableEnemy.followRangeCollider.offset.Multiply(followRangeTrs.lossyScale);
				makeCirclePolygonCollider.circle.radius = awakableEnemy.followRangeCollider.radius * Mathf.Abs(followRangeTrs.lossyScale.x);
				makeCirclePolygonCollider.pointCount = pointsPerPolygonCollider;
				makeCirclePolygonCollider.polygonCollider = gameObject.AddComponent<PolygonCollider2D>();
				makeCirclePolygonCollider.Do ();
				makeCirclePolygonCollider.polygonCollider.usedByComposite = true;
				makeCirclePolygonColliders = makeCirclePolygonColliders.Add(makeCirclePolygonCollider);
			}
			DestroyImmediate(GetComponent<PolygonCollider2D>());
			compositeCollider.GenerateGeometry();
		}

		public virtual void OnDestroy ()
		{
			if (Application.isPlaying)
				return;
			EditorApplication.update -= DoEditorUpdate;
		}
#endif
	}
}