using Extensions;
using UnityEngine;
using TMPro;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class Perk : MonoBehaviour, ISaveableAndLoadable
	{
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int uniqueId;
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public static Perk[] instances = new Perk[0];
		public int cost;
		public TMP_Text costText;
		[SaveAndLoadValue(false)]
		public bool hasPurchased;

#if UNITY_EDITOR
		public virtual void Update ()
		{
			if (Application.isPlaying)
				return;
			costText.text = "" + cost;
		}
#endif

		public virtual void Init ()
		{
			if (hasPurchased)
				Apply ();
		}

		public virtual void Apply ()
		{
		}

		public virtual void Buy ()
		{
			if (!Obelisk.playerIsAtObelisk)
			{
				if (AccountManager.CurrentlyPlaying.CurrentMoney >= cost)
					GameManager.GetSingleton<GameManager>().notificationText.text.text = "You must be at an obelisk to buy perks!";
				else
					GameManager.GetSingleton<GameManager>().notificationText.text.text = "You must be at an obelisk to buy perks! You don't have enough money anyway!";
				GameManager.GetSingleton<GameManager>().notificationText.Do ();
				return;
			}
			if (AccountManager.CurrentlyPlaying.CurrentMoney >= cost)
			{
				GameManager.GetSingleton<Player>().AddMoney (-cost);
				hasPurchased = true;
				Apply ();
				// SaveAndLoadManager.SaveNonSharedData ();
				GameManager.GetSingleton<SaveAndLoadManager>().Save ();
			}
			else
			{
				GameManager.GetSingleton<GameManager>().notificationText.text.text = "You don't have enough money to buy that!";
				GameManager.GetSingleton<GameManager>().notificationText.Do ();
			}
		}
	}
}