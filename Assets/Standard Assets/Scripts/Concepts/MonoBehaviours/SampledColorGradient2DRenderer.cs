using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
using EditorCoroutineWithData = ThreadingUtilities.EditorCoroutineWithData;
#endif
using CoroutineWithData = ThreadingUtilities.CoroutineWithData;

namespace FaceTracking
{
	[ExecuteInEditMode]
	public class SampledColorGradient2DRenderer : MonoBehaviour
	{
		public bool generateRandom;
		public bool update;
		public SampledColorGradient2D sampledColorGradient;
		public SpriteRenderer spriteRenderer;
		public Vector2Int textureSize;
		public float pixelsPerUnit;
		public int colorAreaCount;
		public int blurRadius;
#if UNITY_EDITOR
		EditorCoroutine editorCoroutine;
#endif

#if UNITY_EDITOR
		public virtual void Update ()
		{
			if (Application.isPlaying)
				return;
			if (generateRandom)
			{
				generateRandom = false;
				GenerateRandom ();
			}
			if (update)
			{
				update = false;
				UpdateRenderer ();
			}
		}
#endif

		void GenerateRandom ()
		{
#if UNITY_EDITOR
			if (editorCoroutine != null)
				EditorCoroutineUtility.StopCoroutine(editorCoroutine);
			editorCoroutine = EditorCoroutineUtility.StartCoroutine(GenerateRandomRoutine (), this);
#else
			StartCoroutine(GenerateRandomRoutine ());
#endif
		}

		IEnumerator GenerateRandomRoutine ()
		{
#if UNITY_EDITOR
			EditorCoroutineWithData coroutineWithData = new EditorCoroutineWithData(this, SampledColorGradient2D.GenerateRandom(textureSize, colorAreaCount, blurRadius));
			yield return new ThreadingUtilities.WaitForReturnedValueOfType_Editor<SampledColorGradient2D>(coroutineWithData);
#else
			CoroutineWithData coroutineWithData = new CoroutineWithData(this, SampledColorGradient2D.GenerateRandom(textureSize, colorAreaCount, blurRadius));
			yield return new ThreadingUtilities.WaitForReturnedValueOfType<SampledColorGradient2D>(coroutineWithData);
#endif
			sampledColorGradient = (SampledColorGradient2D) coroutineWithData.result;
		}

		void UpdateRenderer ()
		{
			spriteRenderer.sprite = sampledColorGradient.MakeSprite(textureSize, pixelsPerUnit);
		}
	}
}