using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	public class ActivatableArrow : Arrow
	{
		protected bool isActivated;
		protected bool arrowActionInput;
		protected bool previousArrowActionInput;

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			isActivated = false;
		}

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			arrowActionInput = InputManager.ArrowActionInput;
			if (Time.time - Player.timeOfLastShot >= Time.deltaTime * 2 && arrowActionInput && !previousArrowActionInput)
			{
				int indexOfArrowEntry = GameManager.GetSingleton<Player>().GetArrowEntryIndex(GetType());
				if (indexOfArrowEntry == GameManager.GetSingleton<Player>().currentArrowEntryIndex)
				{
					isActivated = !isActivated;
					if (isActivated)
						Activate ();
					else
						Deactivate ();
				}
			}
			previousArrowActionInput = arrowActionInput;
		}

		public virtual void Activate ()
		{
		}

		public virtual void Deactivate ()
		{
		}
	}
}