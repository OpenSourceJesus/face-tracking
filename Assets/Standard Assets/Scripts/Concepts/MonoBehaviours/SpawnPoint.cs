using UnityEngine;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class SpawnPoint : MonoBehaviour
	{
		public Transform trs;
		public Vector2 anchorPoint;
	}
}