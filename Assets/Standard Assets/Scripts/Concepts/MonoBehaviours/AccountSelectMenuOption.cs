using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Extensions;
using UnityEngine.UI;
using System.Reflection;
using UnityEngine.EventSystems;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class AccountSelectMenuOption : MonoBehaviour
	{
		public Account account = new Account();
		public RectTransform rectTrs;
		public TMP_Text accountNameText;
		public static AccountSelectMenuOption copyAccountSelectMenuOption;
		public GameObject createButtonGo;
		public GameObject accountNameInputFieldGo;
		public GameObject playButtonGo;
		public GameObject startCopyButtonGo;
		public GameObject endCopyButtonGo;
		public GameObject cancelCopyButtonGo;
		public GameObject deleteButtonGo;
		public TemporaryActiveText tempActiveText;
		public CanvasGroup canvasGroup;

		public virtual void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rectTrs == null)
					rectTrs = GetComponent<RectTransform>();
				return;
			}
#endif
		}

		public virtual void StartCreation ()
		{
			GameManager.GetSingleton<AccountSelectMenu>().canvasGroup.interactable = false;
			foreach (AccountSelectMenuOption menuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
			{
				if (menuOption != this)
					menuOption.canvasGroup.interactable = false;
			}
			InputField accountNameInputField = accountNameInputFieldGo.GetComponent<InputField>();
			GameManager.GetSingleton<VirtualKeyboard>().outputToInputField = accountNameInputField;
			GameManager.GetSingleton<VirtualKeyboard>().doneKey.invokeOnPressed.RemoveAllListeners();
			GameManager.GetSingleton<VirtualKeyboard>().doneKey.invokeOnPressed.AddListener(delegate { TryCreate (accountNameInputField.text); });
			GameManager.GetSingleton<VirtualKeyboard>().cancelKey.invokeOnPressed.RemoveAllListeners();
			GameManager.GetSingleton<VirtualKeyboard>().cancelKey.invokeOnPressed.AddListener(delegate { CancelCreate (); });
			GameManager.GetSingleton<VirtualKeyboard>().EnableInput ();
			accountNameInputField.GetType().GetField("m_AllowInput", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(accountNameInputField, true);
			accountNameInputField.GetType().InvokeMember("SetCaretVisible", BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Instance, null, accountNameInputField, null);
			GameManager.GetSingleton<EventSystem>().SetSelectedGameObject(accountNameInputField.gameObject);
		}

		public virtual void CancelCreate ()
		{
			GameManager.GetSingleton<AccountSelectMenu>().canvasGroup.interactable = true;
			GameManager.GetSingleton<VirtualKeyboard>().DisableInput ();
			foreach (AccountSelectMenuOption menuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
				menuOption.canvasGroup.interactable = true;
			accountNameInputFieldGo.SetActive(false);
			createButtonGo.SetActive(true);
		}

		public virtual void TryCreate (string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				tempActiveText.text.text = "Account name can't be empty!";
				tempActiveText.Do ();
				return;
			}
			foreach (Account account in AccountManager.Accounts)
			{
				if (account.Name == name)
				{
					tempActiveText.text.text = "Another account has that name!";
					tempActiveText.Do ();
					return;
				}
			}
			GameManager.GetSingleton<VirtualKeyboard>().DisableInput ();
			accountNameText.text = "Account: " + name;
			account.Name = name;
			// GameManager.GetSingleton<SaveAndLoadManager>().Save ();
			accountNameInputFieldGo.SetActive(false);
			playButtonGo.SetActive(true);
			startCopyButtonGo.SetActive(true);
			deleteButtonGo.SetActive(true);
			GameManager.GetSingleton<AccountSelectMenu>().canvasGroup.interactable = true;
			foreach (AccountSelectMenuOption menuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
				menuOption.canvasGroup.interactable = true;
		}
		
		public virtual void Play ()
		{
			GameManager.GetSingleton<AccountSelectMenu>().gameObject.SetActive(false);
			if (AccountManager.lastUsedAccountIndex == account.index)
				GameManager.GetSingleton<PauseMenu>().Hide ();
			else if (AccountManager.lastUsedAccountIndex != -1)
			{
				SaveAndLoadManager.ResetPersistantValues ();
				AccountManager.lastUsedAccountIndex = account.index;
				GameManager.GetSingleton<GameManager>().LoadScene ("Init");
			}
			else
			{
				AccountManager.lastUsedAccountIndex = account.index;
				GameManager.GetSingleton<SaveAndLoadManager>().LoadMostRecent ();
				GameManager.GetSingleton<GameManager>().PauseGame (false);
			}
		}

		public virtual void StartCopy ()
		{
			copyAccountSelectMenuOption = this;
			foreach (AccountSelectMenuOption accountSelectMenuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
			{
				if (accountSelectMenuOption != this)
				{
					accountSelectMenuOption.endCopyButtonGo.SetActive(true);
					if (string.IsNullOrEmpty(accountSelectMenuOption.account.Name))
					{
						accountSelectMenuOption.createButtonGo.SetActive(false);
						accountSelectMenuOption.accountNameInputFieldGo.SetActive(false);
					}
					else
					{
						accountSelectMenuOption.playButtonGo.SetActive(false);
						accountSelectMenuOption.startCopyButtonGo.SetActive(false);
						accountSelectMenuOption.deleteButtonGo.SetActive(false);
					}
				}
			}
		}

		public virtual void CancelCopy ()
		{
			foreach (AccountSelectMenuOption accountSelectMenuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
			{
				if (accountSelectMenuOption != this)
				{
					accountSelectMenuOption.endCopyButtonGo.SetActive(false);
					if (string.IsNullOrEmpty(accountSelectMenuOption.account.Name))
						accountSelectMenuOption.createButtonGo.SetActive(true);
					else
					{
						accountSelectMenuOption.playButtonGo.SetActive(true);
						accountSelectMenuOption.startCopyButtonGo.SetActive(true);
						accountSelectMenuOption.deleteButtonGo.SetActive(true);
					}
				}
			}
		}

		public virtual void EndCopy ()
		{
			account = copyAccountSelectMenuOption.account;
			account.Name += " (Copy)";
			accountNameText.text = "Account: " + account.Name;
			account.index = rectTrs.GetSiblingIndex();
			// GameManager.GetSingleton<SaveAndLoadManager>().Save ();
			copyAccountSelectMenuOption.cancelCopyButtonGo.SetActive(false);
			foreach (AccountSelectMenuOption accountSelectMenuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
			{
				if (accountSelectMenuOption != this)
				{
					accountSelectMenuOption.endCopyButtonGo.SetActive(false);
					if (string.IsNullOrEmpty(accountSelectMenuOption.account.Name))
						accountSelectMenuOption.createButtonGo.SetActive(true);
					else
					{
						accountSelectMenuOption.playButtonGo.SetActive(true);
						accountSelectMenuOption.startCopyButtonGo.SetActive(true);
						accountSelectMenuOption.deleteButtonGo.SetActive(true);
					}
				}
			}
		}

		public virtual void Delete ()
		{
			GameManager.GetSingleton<SaveAndLoadManager>().Delete (account.index);
			account.Name = "";
			account.index = rectTrs.GetSiblingIndex();
			// GameManager.GetSingleton<SaveAndLoadManager>().Save ();
			if (AccountManager.lastUsedAccountIndex == account.index)
			{
				AccountManager.lastUsedAccountIndex = -1;
				SaveAndLoadManager.ResetPersistantValues ();
			}
			AccountSelectMenu.Init ();
		}
	}
}
