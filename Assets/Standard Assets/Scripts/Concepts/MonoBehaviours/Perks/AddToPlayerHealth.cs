namespace FaceTracking
{
	public class AddToPlayerHealth : Perk
	{
		public override void Apply ()
		{
			GameManager.GetSingleton<Player>().maxHp ++;
			GameManager.GetSingleton<Player>().Hp ++;
			Instantiate(GameManager.GetSingleton<Player>().lifeIconsParent.GetChild(0).gameObject, GameManager.GetSingleton<Player>().lifeIconsParent);
		}
	}
}