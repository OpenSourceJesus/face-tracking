namespace FaceTracking
{
	public class AddToSkips : Perk
	{
		public override void Buy ()
		{
			if (AccountManager.CurrentlyPlaying.CurrentMoney > cost)
			{
				base.Buy ();
				SkipManager.skipPoints ++;
			}
		}
	}
}