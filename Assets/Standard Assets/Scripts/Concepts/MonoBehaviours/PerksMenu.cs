using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class PerksMenu : MonoBehaviour
	{
		public virtual void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.singletons.Remove(GetType());
			GameManager.singletons.Add(GetType(), this);
			gameObject.SetActive(false);
		}

		public virtual void ShowSkipsScreen ()
		{
			GameManager.GetSingleton<WorldMap>().Open ();
		}

		public virtual void HideSkipsScreen ()
		{
			GameManager.GetSingleton<WorldMap>().Close ();
		}
	}
}