﻿using UnityEngine;
using Extensions;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class AccountSelectMenu : SaveAndLoadObject, ISaveableAndLoadable
	{
		public AccountSelectMenuOption[] menuOptions;
		public CanvasGroup canvasGroup;

		public override void Setup ()
		{
			saveables = new ISaveableAndLoadable[0];
			foreach (AccountSelectMenuOption menuOption in menuOptions)
				saveables = saveables.Add(menuOption.account);
			base.Setup ();
			// print(saveEntries[0].memberEntries);
		}

		public static void Init ()
		{
			GameManager.GetSingleton<VirtualKeyboard>().DisableInput ();
			int numberOfEmptyAccounts = 0;
			AccountSelectMenuOption accountSelectMenuOption;
			Account account;
			for (int i = 0; i < GameManager.GetSingleton<AccountSelectMenu>().menuOptions.Length; i ++)
			{
				accountSelectMenuOption = GameManager.GetSingleton<AccountSelectMenu>().menuOptions[i];
				account = accountSelectMenuOption.account;
				if (string.IsNullOrEmpty(account.Name))
				{
					numberOfEmptyAccounts ++;
					accountSelectMenuOption.accountNameText.text = "Empty Account #" + numberOfEmptyAccounts;
				}
				else
				{
					accountSelectMenuOption.accountNameText.text = "Account: " + account.Name;
					accountSelectMenuOption.createButtonGo.SetActive(false);
					accountSelectMenuOption.playButtonGo.SetActive(true);
					accountSelectMenuOption.startCopyButtonGo.SetActive(true);
					accountSelectMenuOption.deleteButtonGo.SetActive(true);
				}
			}
		}

#if UNITY_EDITOR
		public virtual void Start ()
		{
			if (!Application.isPlaying)
			{
				menuOptions = GetComponentsInChildren<AccountSelectMenuOption>();
				return;
			}
		}
#endif
	}
}
