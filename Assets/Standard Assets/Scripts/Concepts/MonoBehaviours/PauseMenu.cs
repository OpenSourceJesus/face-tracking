using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace FaceTracking
{
	public class PauseMenu : MonoBehaviour
	{
		public Canvas canvas;
		public Button[] switchToSectionButtons;
		public Section section;

		public virtual void Show ()
		{
			if (GameManager.GetSingleton<PauseMenu>() != this)
			{
				GameManager.GetSingleton<PauseMenu>().Show ();
				return;
			}
			SetSection (section.GetHashCode());
			gameObject.SetActive(true);
			GameManager.GetSingleton<GameManager>().PauseGame (true);
			if (section == Section.WorldMap)
				OpenWorldMap ();
			else if (section == Section.Quests)
				GameManager.GetSingleton<QuestManager>().ShowQuestsScreen ();
			else if (section == Section.Perks)
				GameManager.GetSingleton<PerksMenu>().gameObject.SetActive(true);
			StartCoroutine(UpdateRoutine ());
		}

		int switchMenuSectionInput;
		int previousSwitchMenuSectionInput;
		public virtual IEnumerator UpdateRoutine ()
		{
			do
			{
				switchMenuSectionInput = InputManager.SwitchMenuSectionInput;
				if (switchMenuSectionInput != 0 && previousSwitchMenuSectionInput != switchMenuSectionInput)
				{
					if (section.GetHashCode() + switchMenuSectionInput < 0)
						SetSection (Enum.GetValues(typeof(Section)).Length - 1);
					else if (section.GetHashCode() + switchMenuSectionInput >= Enum.GetValues(typeof(Section)).Length)
						SetSection (0);
					else
						SetSection (section.GetHashCode() + switchMenuSectionInput);
				}
				previousSwitchMenuSectionInput = switchMenuSectionInput;
				yield return new WaitForEndOfFrame();
			} while (true);
		}

		public virtual void Hide ()
		{
			if (GameManager.GetSingleton<PauseMenu>() != this)
			{
				GameManager.GetSingleton<PauseMenu>().Hide ();
				return;
			}
			gameObject.SetActive(false);
			if (section == Section.WorldMap)
				CloseWorldMap ();
			else if (section == Section.Quests)
				GameManager.GetSingleton<QuestManager>().HideQuestsScreen ();
			else if (section == Section.Perks)
				GameManager.GetSingleton<PerksMenu>().gameObject.SetActive(false);
			else
			{
				section = Section.WorldMap;
				canvas.enabled = true;
				GameManager.GetSingleton<AccountSelectMenu>().gameObject.SetActive(false);
			}
			GameManager.GetSingleton<GameManager>().PauseGame (false);
			StopCoroutine(UpdateRoutine ());
		}

		public virtual void SetSection (int index)
		{
			ColorBlock sectionButtonColors;
			foreach (Button _switchSectionButton in switchToSectionButtons)
			{
				sectionButtonColors = _switchSectionButton.colors;
				sectionButtonColors.colorMultiplier = 1;
				_switchSectionButton.colors = sectionButtonColors;
			}
			section = (Section) Enum.ToObject(typeof(Section), index);
			Button switchSectionButton = switchToSectionButtons[index];
			sectionButtonColors = switchSectionButton.colors;
			sectionButtonColors.colorMultiplier /= 2;
			switchSectionButton.colors = sectionButtonColors;
			if (section == Section.WorldMap)
				OpenWorldMap ();
			else
				CloseWorldMap ();
			if (section == Section.Quests)
				GameManager.GetSingleton<QuestManager>().ShowQuestsScreen ();
			else
				GameManager.GetSingleton<QuestManager>().HideQuestsScreen ();
			if (section == Section.Perks)
				GameManager.GetSingleton<PerksMenu>().gameObject.SetActive(true);
			else
				GameManager.GetSingleton<PerksMenu>().gameObject.SetActive(false);
			if (section == Section.MainMenu)
			{
				canvas.enabled = false;
				GameManager.GetSingleton<AccountSelectMenu>().gameObject.SetActive(true);
			}
			else
			{
				canvas.enabled = true;
				GameManager.GetSingleton<AccountSelectMenu>().gameObject.SetActive(false);
			}
		}
		
		public virtual void OpenWorldMap ()
		{
			GameManager.GetSingleton<WorldMap>().Open ();
			canvas.worldCamera = GameManager.GetSingleton<WorldMap>().worldMapCamera.camera;
		}

		public virtual void CloseWorldMap ()
		{
			GameManager.GetSingleton<WorldMap>().Close ();
			canvas.worldCamera = GameManager.GetSingleton<GameCamera>().camera;
		}

		public enum Section
		{
			WorldMap,
			Quests,
			Perks,
			MainMenu
		}
	}
}