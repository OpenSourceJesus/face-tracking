using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using Extensions;
using TMPro;

namespace FaceTracking
{
	public class RacingMinigame : Minigame, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public Timer timer;
		public TMP_Text timerText;
		public float timeRemainingIntoScoreMultiplier;
		bool isPlaying;

		public override void Begin ()
		{
			base.Begin ();
			GameManager.updatables = GameManager.updatables.Add(this);
			timer.Reset ();
			timer.Start ();
			GameManager.onGameScenesLoaded += delegate { GameManager.GetSingleton<RacingMinigame>().ShowRetryScreen (); };
			isPlaying = true;
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public virtual void DoUpdate ()
		{
			timerText.text = timer.TimeElapsed.ToString("0.0");
		}

		public virtual void End ()
		{
			timer.Stop ();
			DoUpdate ();
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (isPlaying)
				AddScore ((int) (Mathf.Clamp(timer.timeRemaining, 0, timer.duration) * timeRemainingIntoScoreMultiplier));
			ShowRetryScreen ();
			isPlaying = false;
		}
	}
}