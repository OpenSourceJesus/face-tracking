using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FaceTracking
{    
	public class HurtAnimation : StateMachineBehaviour
	{
		public override void OnStateExit (Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
		{
			GameManager.GetSingleton<Player>().isHurting = false;
		}
	}
}