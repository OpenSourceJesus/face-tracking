using UnityEngine;
using UnityEngine.InputSystem;

namespace FaceTracking
{
	public class WorldMapCamera : CameraScript
	{
		public float sizeMultiplier;
		public float zoomRate;
		public FloatRange sizeMultiplierRange;
		[SerializeField]
		Vector2 initViewSize;
		Rect previousViewRect;
		float zoomInput;
		Vector2 worldMousePosition;
		Vector2 previousWorldMousePosition;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				initViewSize = viewSize;
				return;
			}
#endif
			if (GameManager.singletons.ContainsKey(typeof(CameraScript)))
				GameManager.singletons[typeof(CameraScript)] = this;
			else
				GameManager.singletons.Add(typeof(CameraScript), this);
		}

		public override void DoUpdate ()
		{
			zoomInput = InputManager.ZoomInput;
			HandleViewSize ();
			base.DoUpdate ();
			previousViewRect = viewRect;
			previousWorldMousePosition = InputManager.GetWorldMousePosition();
		}

		public override void HandlePosition ()
		{
			if (zoomInput != 0 && sizeMultiplier > sizeMultiplierRange.min && sizeMultiplier < sizeMultiplierRange.max)
			{
				worldMousePosition = InputManager.GetWorldMousePosition();
				trs.position -= (Vector3) ((worldMousePosition - viewRect.center) - (previousWorldMousePosition - previousViewRect.center));
				if (GameManager.GetSingleton<GameManager>().worldMapTutorialConversation.lastStartedDialog == GameManager.GetSingleton<GameManager>().worldMapZoomViewTutorialDialog)
					GameManager.GetSingleton<GameManager>().DeactivateGoForever (GameManager.GetSingleton<GameManager>().worldMapTutorialConversation.gameObject);
			}
			base.HandlePosition ();
		}

		public override void HandleViewSize ()
		{
			sizeMultiplier = Mathf.Clamp(sizeMultiplier + zoomInput * zoomRate * Time.unscaledDeltaTime, sizeMultiplierRange.min, sizeMultiplierRange.max);
			viewSize = initViewSize * sizeMultiplier;
			base.HandleViewSize ();
		}

		CameraView previousCameraView;
		public virtual void SetView (CameraView cameraView)
		{
			if (previousCameraView != null)
				previousCameraView.gameObject.SetActive(false);
			trs.position = cameraView.trs.position;
			viewSize = cameraView.trs.localScale;
			base.HandleViewSize ();
			cameraView.gameObject.SetActive(true);
			previousCameraView = cameraView;
		}

		public virtual void SetView (string cameraViewName)
		{
			SetView (CameraView.cameraViewDict[cameraViewName]);
		}

		public virtual void Enable ()
		{
			if (GameManager.GetSingleton<WorldMapCamera>() != this)
			{
				GameManager.GetSingleton<WorldMapCamera>().Enable ();
				return;
			}
			enabled = true;
		}

		public virtual void Disable ()
		{
			if (GameManager.GetSingleton<WorldMapCamera>() != this)
			{
				GameManager.GetSingleton<WorldMapCamera>().Disable ();
				return;
			}
			enabled = false;
		}
	}
}