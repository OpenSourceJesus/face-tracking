using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DialogAndStory;
using System;

namespace FaceTracking
{
	public class ArrowCollectible : Collectible
	{
		public static Dictionary<Type, Conversation> tutorialConversationsDict = new Dictionary<Type, Conversation>();
		public Arrow arrowType;
		public Conversation tutorialConversation;

		public override void Awake ()
		{
			base.Awake ();
			if (!tutorialConversationsDict.ContainsKey(arrowType.GetType()))
				tutorialConversationsDict.Add(arrowType.GetType(), tutorialConversation);
		}

		public override void OnCollected ()
		{
			base.OnCollected ();
			Player.ArrowEntry arrowEntry = GameManager.GetSingleton<Player>().GetArrowEntry(arrowType.GetType());
			arrowEntry.isOwned = true;
			arrowEntry.menuOptionToSwitchToMe.SetActive(true);
			if (tutorialConversation != null)
				GameManager.GetSingleton<DialogManager>().StartConversation (tutorialConversation);
		}
	}
}