#if UNITY_EDITOR
using UnityEngine;
using FaceTracking;
using UnityEditor;

//[ExecuteInEditMode]
public class TeleportPlayerToMouse : EditorScript
{
    public virtual void Do ()
    {
        GameManager.GetSingleton<Player>().trs.position = GetMousePositionInWorld();
    }
}

[CustomEditor(typeof(TeleportPlayerToMouse))]
public class TeleportPlayerToMouseEditor : EditorScriptEditor
{
}
#endif
#if !UNITY_EDITOR
public class TeleportPlayerToMouse : EditorScript
{
}
#endif