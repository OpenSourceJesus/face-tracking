using UnityEngine;
using System;

public class ToggleExecuteInEditMode : EditorScript
{
	public ScriptEntry[] scriptEntries = new ScriptEntry[0];

	public override void DoEditorUpdate ()
	{

	}

	[Serializable]
	public class ScriptEntry
	{
		public MonoBehaviour script;
	}
}