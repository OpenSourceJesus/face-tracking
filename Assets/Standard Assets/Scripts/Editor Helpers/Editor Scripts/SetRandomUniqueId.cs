#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class SetRandomUniqueId : EditorScript
{
	void Awake ()
	{
		GetComponent<IIdentifiable>().UniqueId = Random.Range(int.MinValue, int.MaxValue);
		DestroyImmediate(this);
	}
}
#endif