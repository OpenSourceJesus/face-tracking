#if UNITY_EDITOR
using FaceTracking;
using UnityEngine;
using UnityEditor;
using Extensions;

public class ExploreWorldMap : EditorScript
{
	public float distanceToExploreFromObelisks;

	public virtual void Do ()
	{
		WorldMap.exploredCellPositions.Clear();
		foreach (Vector2Int cellPosition in GameManager.GetSingleton<World>().cellBoundsRect.allPositionsWithin)
		{
			foreach (Obelisk obelisk in Obelisk.instances)
			{
				if ((cellPosition.ToVec2() - (Vector2) obelisk.trs.position).sqrMagnitude <= distanceToExploreFromObelisks * distanceToExploreFromObelisks)
				{
					WorldMap.exploredCellPositions.Add(cellPosition);
					break;
				}	
			}
		}
		GameManager.GetSingleton<WorldMap>().minExploredCellPosition = GameManager.GetSingleton<World>().cellBoundsRect.min;
		GameManager.GetSingleton<WorldMap>().maxExploredCellPosition = GameManager.GetSingleton<World>().cellBoundsRect.max;
	}
}

[CustomEditor(typeof(ExploreWorldMap))]
public class ExploreWorldMapEditor : EditorScriptEditor
{
}
#endif
#if !UNITY_EDITOR
using UnityEngine;

public class ExploreWorldMap : MonoBehaviour
{
}
#endif