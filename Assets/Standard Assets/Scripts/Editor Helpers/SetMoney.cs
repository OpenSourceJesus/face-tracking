#if UNITY_EDITOR
using UnityEngine;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class SetMoney : MonoBehaviour
	{
		public bool update;
		public int amount;

		public virtual void Update ()
		{
			if (!update)
				return;
			update = false;
			GameManager.GetSingleton<Player>().AddMoney (amount - AccountManager.CurrentlyPlaying.CurrentMoney);
			DestroyImmediate(this);
		}
	}
}
#endif