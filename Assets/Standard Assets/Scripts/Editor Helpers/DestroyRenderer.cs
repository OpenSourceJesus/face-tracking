﻿using UnityEngine;

public class DestroyRenderer : MonoBehaviour
{
	void Start ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		Destroy(GetComponent<Renderer>());
		Destroy(this);
	}
}