#if UNITY_EDITOR
using UnityEngine;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class AddMoney : MonoBehaviour
	{
		public bool update;
		public int amount;

		public virtual void Update ()
		{
			if (!update)
				return;
			update = false;
			GameManager.GetSingleton<Player>().AddMoney (amount);
			DestroyImmediate(this);
		}
	}
}
#endif