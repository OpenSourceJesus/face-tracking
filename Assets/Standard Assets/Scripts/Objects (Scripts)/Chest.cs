using UnityEngine;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class Chest : Collectible
	{
		public int money;
		
		public override void OnCollected ()
		{
			base.OnCollected ();
			GameManager.GetSingleton<Player>().AddMoney (money);
		}
	}
}