﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
    //[ExecuteInEditMode]
	public class WindArrow : Arrow
	{
		public WindExplosion windExplosionPrefab;

		public override void OnCollisionEnter2D (Collision2D coll)
		{
			base.OnCollisionEnter2D (coll);
			Destroy(gameObject);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			WindExplosion windExplosion = GameManager.GetSingleton<ObjectPool>().SpawnComponent<WindExplosion>(windExplosionPrefab.prefabIndex, trs.position, Quaternion.identity);
			windExplosion.SetForce (speed);
			windExplosion.collider.enabled = true;
		}
	}
}