﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	public class SludgeArrow : ActivatableArrow
	{
		protected bool inSludge;
		public float linearDragIncreaseRate;
		public float angularDragIncreaseRate;

		public override void Activate ()
		{
			if (inSludge)
				return;
			inSludge = true;
			StartCoroutine(SludgeRoutine ());
			StartCoroutine(MakeCollidable ());
		}

		public virtual IEnumerator MakeCollidable ()
		{
			gameObject.layer = LayerMask.NameToLayer("Sludge Arrow");
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.layerMask = LayerMask.GetMask("Player");
			contactFilter.useLayerMask = true;
			Physics2D.IgnoreCollision(GameManager.GetSingleton<Player>().collider, collider, true);
			yield return new WaitUntil(() => (collider.OverlapCollider(contactFilter, new Collider2D[1]) == 0));
			Physics2D.IgnoreCollision(GameManager.GetSingleton<Player>().collider, collider, false);
		}

		public virtual IEnumerator SludgeRoutine ()
		{
			do
			{
				rigid.drag += linearDragIncreaseRate * Time.deltaTime;
				rigid.angularDrag += angularDragIncreaseRate * Time.deltaTime;
				yield return new WaitForEndOfFrame();
			} while (true);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			StopCoroutine(SludgeRoutine ());
			StopCoroutine(MakeCollidable ());
			gameObject.layer = LayerMask.NameToLayer("Arrow");
			rigid.drag = 0;
			rigid.angularDrag = 0;
			inSludge = false;
		}
	}
}