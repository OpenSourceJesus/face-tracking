﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FaceTracking
{
	public class PullArrow : ActivatableArrow
	{
		public float pullSpeed;
		Enemy stuckInEnemy;

		public override void Activate ()
		{
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.layerMask = LayerMask.GetMask("Player");
			contactFilter.useLayerMask = true;
			if (collider.OverlapCollider(contactFilter, new Collider2D[1]) == 1)
				Destroy(gameObject);
			else
				StartCoroutine(PullRoutine ());
		}

		public virtual IEnumerator PullRoutine ()
		{
			rigid.gravityScale = 0;
			GameManager.GetSingleton<Player>().canMoveAndJump = false;
			do
			{
				if (collider.isTrigger)
				{
					stuckInEnemy = trs.parent.GetComponent<Enemy>();
					if (stuckInEnemy != null)
						stuckInEnemy.velocityEffectors_Vector2Dict["Pull Arrow"].effect = (Vector2) (GameManager.GetSingleton<Player>().trs.position - trs.position).normalized * pullSpeed;
				}
				else
				{
					rigid.drag = 0;
					rigid.angularDrag = float.MaxValue;
					rigid.velocity = (Vector2) (GameManager.GetSingleton<Player>().trs.position - trs.position).normalized * pullSpeed;
				}
				GameManager.GetSingleton<Player>().velocityEffectors_Vector2Dict["Pull Arrow"].effect = (Vector2) (trs.position - GameManager.GetSingleton<Player>().trs.position).normalized * pullSpeed;
				yield return new WaitForEndOfFrame();
			} while (true);
		}

		public override void Deactivate ()
		{
			GameManager.GetSingleton<Player>().CurrentArrowEntry.canShoot = false;
			Destroy (gameObject);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			rigid.gravityScale = 1;
			StopCoroutine(PullRoutine ());
			GameManager.GetSingleton<Player>().canMoveAndJump = true;
			GameManager.GetSingleton<Player>().velocityEffectors_Vector2Dict["Pull Arrow"].effect = Vector2.zero;
			if (stuckInEnemy != null)
				stuckInEnemy.velocityEffectors_Vector2Dict["Pull Arrow"].effect = Vector2.zero;
		}

		public override void OnCollisionEnter2D (Collision2D coll)
		{
			if (coll.gameObject.GetComponent<Enemy>() != null)
			{
				collider.isTrigger = true;
				rigid.bodyType = RigidbodyType2D.Kinematic;
				rigid.velocity = Vector2.zero;
				trs.SetParent(coll.transform);
			}
		}
	}
}