﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using DelayedDespawn = FaceTracking.ObjectPool.DelayedDespawn;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class Bullet : MonoBehaviour, ISpawnable
	{
		public Transform trs;
		public Rigidbody2D rigid;
		public Collider2D collider;
		public virtual bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
        public float moveSpeed;
        public float lifetime;
        DelayedDespawn delayedDespawn;
		public SpriteRenderer spriteRenderer;
		public Color canRetargetColor;
		public Color cantRetargetColor;
		public bool despawnOnHit;
		[HideInInspector]
		public int timesRetargeted;
		[HideInInspector]
		public bool hasFinishedRetargeting;
		[HideInInspector]
		public Vector2 boundsSize = VectorExtensions.NULL3;

        public virtual void OnEnable ()
        {
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (collider == null)
					collider = GetComponent<Collider2D>();
				if (boundsSize == (Vector2) VectorExtensions.NULL3)
					boundsSize = collider.GetSize(trs);
				return;
			}
#endif
            rigid.velocity = trs.up * moveSpeed;
			if (lifetime > 0)
            	delayedDespawn = GameManager.GetSingleton<ObjectPool>().DelayDespawn (prefabIndex, gameObject, trs, lifetime);
        }

        public virtual void OnTriggerEnter2D (Collider2D other)
        {
            if (other == GameManager.GetSingleton<Player>().collider)
                GameManager.GetSingleton<Player>().TakeDamage ();
			if (despawnOnHit)
				Despawn ();
        }

		public virtual void Despawn ()
		{
			GameManager.GetSingleton<ObjectPool>().CancelDelayedDespawn (delayedDespawn);
			GameManager.GetSingleton<ObjectPool>().Despawn (prefabIndex, gameObject, trs);
		}

		public virtual void Retarget (Vector2 direction, bool lastRetarget = false)
		{
			if (lastRetarget && hasFinishedRetargeting)
				return;
			trs.up = direction;
			rigid.velocity = trs.up * moveSpeed;
			timesRetargeted ++;
			if (lastRetarget)
			{
				hasFinishedRetargeting = true;
				spriteRenderer.color = cantRetargetColor;
				spriteRenderer.sortingOrder --;
			}
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			timesRetargeted = 0;
			if (hasFinishedRetargeting)
			{
				hasFinishedRetargeting = false;
				spriteRenderer.color = canRetargetColor;
				spriteRenderer.sortingOrder ++;
			}
			StopAllCoroutines();
		}
	}
}