using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;
using Random = UnityEngine.Random;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
using TMPro;
using DialogAndStory;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class Player : PlatformerEntity, IDestructable, ISaveableAndLoadable
	{
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int uniqueId;
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public ArrowEntry[] arrowEntries;
		[HideInInspector]
		public int currentArrowEntryIndex;
		public ArrowEntry CurrentArrowEntry
		{
			get
			{
				return arrowEntries[currentArrowEntryIndex];
			}
			set
			{
				for (int i = 0; i < arrowEntries.Length; i ++)
				{
					if (arrowEntries[i].arrowPrefab == value.arrowPrefab)
					{
						currentArrowEntryIndex = i;
						break;
					}
				}
			}
		}
		public Timer reloadTimer;
		public float shootSpeed;
		[HideInInspector]
		public bool canShoot = true;
		Vector2 aimVector;
		public float arrowLifetime;
		float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public uint maxHp;
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		// public CircularMenu arrowMenu;
		public LineRenderer aimingVisualizer;
		public LayerMask whatArrowsCollideWith;
		bool arrowHasLeftBody;
		[HideInInspector]
		public bool canTeleport;
		public AudioClip[] shootAudioClips = new AudioClip[0];
		public Animator anim;
		bool isUnderwater;
		[HideInInspector]
		public bool isHurting;
		public GameObject lifeIcon;
		public Transform lifeIconsParent;
		// [HideInInspector]
		[SaveAndLoadValue(false)]
		public Vector2 spawnPosition;
		[HideInInspector]
		[SerializeField]
		float arrowHeight;
		[HideInInspector]
		[SerializeField]
		float arrowDrag;
#if UNITY_EDITOR
		public bool ownAllArrows;
#endif
		public Transform aimerTrs;
		public Transform shootSpawnPoint;
		public TMP_Text moneyText;
		public static float timeOfLastShot;
		public TemporaryActiveObject moneyChangedIndicator;
		public TMP_Text moneyChangedAmountText;
		public static int addToMoneyOnSave;
		// Dictionary<Collider2D, Vector2[]> collisionNormalsDict = new Dictionary<Collider2D, Vector2[]>();
		// public float crushingAngle;
		int switchArrowInput;
		int preivousSwitchArrowInput;

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				EditorPrefs.SetFloat("Arrow size.x", GetArrowEntry(typeof(Arrow)).arrowPrefab.collider.GetSize().x);
				EditorPrefs.SetFloat("Arrow size.y", GetArrowEntry(typeof(Arrow)).arrowPrefab.collider.GetSize().y);
				EditorPrefs.SetFloat("Arrow drag", GetArrowEntry(typeof(Arrow)).arrowPrefab.rigid.drag);
				Init ();
				return;
			}
			else
			{
				if (ownAllArrows)
				{
					foreach (ArrowEntry arrowEntry in arrowEntries)
					{
						arrowEntry.isOwned = true;
						arrowEntry.menuOptionToSwitchToMe.SetActive(true);
					}
				}
			}
#endif
			Init ();
			hp = maxHp;
			for (int i2 = 1; i2 < maxHp; i2 ++)
				Instantiate(lifeIcon, lifeIconsParent);
			// int i = 0;
			// foreach (ArrowEntry arrowEntry in arrowEntries)
			// {
			// 	arrowMenu.options[i].button.onClick.AddListener(delegate { SwitchArrowType (arrowEntry); });
			// 	arrowEntry.menuOptionToSwitchToMe.SetActive(arrowEntry.isOwned);
			// 	i ++;
			// }
		}

		public virtual void Init ()
		{
			Arrow arrowPrefab = GetArrowEntry(typeof(Arrow)).arrowPrefab;
			float arrowWidth = arrowPrefab.collider.GetSize().x;
			arrowHeight = arrowPrefab.collider.GetSize().y;
			arrowDrag = arrowPrefab.rigid.drag;
#if UNITY_EDITOR
			arrowWidth = EditorPrefs.GetFloat("Arrow size.x", arrowWidth);
			arrowHeight = EditorPrefs.GetFloat("Arrow size.y", arrowHeight);
			arrowDrag = EditorPrefs.GetFloat("Arrow drag", arrowDrag);
#endif
			aimingVisualizer.startWidth = arrowWidth;
			aimingVisualizer.endWidth = aimingVisualizer.startWidth;
			spawnPosition = trs.position;
		}

		public virtual void SwitchArrowType (ArrowEntry arrowEntry)
		{
			if (arrowEntry.isOwned)
			{
				Conversation tutorialConversation = null;
				if (ArrowCollectible.tutorialConversationsDict.TryGetValue(arrowEntry.arrowPrefab.GetType(), out tutorialConversation) && tutorialConversation != null && tutorialConversation.lastStartedDialog != null)
					GameManager.GetSingleton<DialogManager>().EndDialog (ArrowCollectible.tutorialConversationsDict[arrowEntry.arrowPrefab.GetType()].lastStartedDialog);
				CurrentArrowEntry = arrowEntry;
			}
		}

		public override void DoUpdate ()
		{
			if (GameManager.GetSingleton<PauseMenu>().gameObject.activeSelf || GameManager.GetSingleton<AccountSelectMenu>().gameObject.activeSelf)
				return;
			base.DoUpdate ();
			HandleAiming ();
			HandleShooting ();
			HandleSwitchArrowType ();
			HandleArrowReturnNotifications ();
		}

		public virtual void HandleSwitchArrowType ()
		{
			switchArrowInput = InputManager.SwitchArrowInput;
			if (switchArrowInput != -1 && preivousSwitchArrowInput != switchArrowInput)
				SwitchArrowType (arrowEntries[switchArrowInput]);
			preivousSwitchArrowInput = switchArrowInput;
		}
		
		Vector2 currentPosition;
		Vector2 currentVelocity;
		float currentGravityScale;
		float currentDrag;
		List<Vector3> positions = new List<Vector3>();
		bool wasPreviouslyInSpiderWeb;
		RaycastHit2D hit;
		RaycastHit2D[] hits = new RaycastHit2D[1];
		bool wasPreviouslyInWater;
		public virtual void HandleAiming ()
		{
			aimVector = InputManager.AimInput;
			currentGravityScale = 1;
			currentDrag = arrowDrag;
			currentPosition = shootSpawnPoint.position;
			currentVelocity = (aimVector * shootSpeed) + rigid.velocity;
			positions.Clear();
			positions.Add(currentPosition);
			wasPreviouslyInSpiderWeb = false;
			wasPreviouslyInWater = false;
			do
			{
				if (!GameManager.gameModifierDict["Unslowable Pull Arrows"].isActive || CurrentArrowEntry.arrowPrefab.GetType() != typeof(PullArrow))
				{
					contactFilter = new ContactFilter2D();
					contactFilter.useTriggers = true;
					contactFilter.layerMask = LayerMask.GetMask("Spider Web");
					contactFilter.useLayerMask = true;
					if (!wasPreviouslyInSpiderWeb && Physics2DExtensions.LinecastWithWidth(currentPosition - currentVelocity.normalized * arrowHeight / 2, currentPosition + currentVelocity.normalized * arrowHeight / 2, aimingVisualizer.startWidth, contactFilter, hits) > 0)
					{
						currentPosition = hits[0].point;
						currentVelocity = Vector2.zero;
						wasPreviouslyInSpiderWeb = true;
					}
					else
					{
						contactFilter = new ContactFilter2D();
						contactFilter.useTriggers = true;
						contactFilter.layerMask = LayerMask.GetMask("Water");
						contactFilter.useLayerMask = true;
						if (Physics2DExtensions.LinecastWithWidth(currentPosition - currentVelocity.normalized * arrowHeight / 2, currentPosition + currentVelocity.normalized * arrowHeight / 2, aimingVisualizer.startWidth, contactFilter, hits) > 0)
						{
							if (!wasPreviouslyInWater)
							{
								currentDrag += GameManager.GetSingleton<Water>().addToLinearDrag;
								currentGravityScale -= GameManager.GetSingleton<Water>().subtractFromGravityScale;
								wasPreviouslyInWater = true;
							}
						}
						else if (wasPreviouslyInWater)
						{
							currentDrag -= GameManager.GetSingleton<Water>().addToLinearDrag;
							currentGravityScale += GameManager.GetSingleton<Water>().subtractFromGravityScale;
							wasPreviouslyInWater = false;
						}
					}
				}
				currentVelocity += Physics2D.gravity * currentGravityScale * Time.fixedDeltaTime;
				currentVelocity *= (1f - Time.fixedDeltaTime * currentDrag);
				currentPosition += currentVelocity * Time.fixedDeltaTime;
				hit = Physics2DExtensions.LinecastWithWidth(positions[positions.Count - 1], currentPosition, aimingVisualizer.startWidth, whatArrowsCollideWith);
				if (hit.collider != null)
				{
					positions.Add(hit.point);
					break;
				}
				if (positions.Contains(currentPosition))
					break;
				positions.Add(currentPosition);
			} while (GameManager.GetSingleton<GameCamera>().viewRect.Contains(currentPosition));
			aimingVisualizer.positionCount = positions.Count;
			aimingVisualizer.SetPositions(positions.ToArray());
			if (aimVector == Vector2.zero)
				aimerTrs.up = Vector3.forward;
			else
				aimerTrs.up = aimVector;
		}

		bool shootInput;
		bool previousShootInput;
		public virtual void HandleShooting ()
		{
			shootInput = InputManager.ShootInput;
			if (shootInput && reloadTimer.timeRemaining <= 0 && canShoot && CurrentArrowEntry.holdingCount > 0 && CurrentArrowEntry.isOwned && CurrentArrowEntry.canShoot)
				Shoot (aimVector);
			else if (!shootInput && previousShootInput)
				CurrentArrowEntry.canShoot = true;
			previousShootInput = shootInput;
		}

		public virtual void HandleArrowReturnNotifications ()
		{
			foreach (ArrowEntry arrowEntry in arrowEntries)
			{
				if (arrowEntry.isOwned && arrowEntry.holdingCount == 0)
				{
					arrowEntry.returnNotification.gameObject.SetActive(true);
					arrowEntry.returnNotification.fillAmount = arrowEntry.arrows[0].delayedDespawn.timeRemaining / arrowEntry.arrows[0].delayedDespawn.duration;
				}
				else
					arrowEntry.returnNotification.gameObject.SetActive(false);
			}
		}

		public virtual void Shoot (Vector2 shoot)
		{
			arrowHasLeftBody = false;
			timeOfLastShot = Time.time;
			Arrow arrow = GameManager.GetSingleton<ObjectPool>().SpawnComponent<Arrow>(CurrentArrowEntry.arrowPrefab.prefabIndex, shootSpawnPoint.position, Quaternion.LookRotation(Vector3.forward, aimVector));
			arrow.rigid.velocity = aimVector * shootSpeed + rigid.velocity;
			arrow.speed = arrow.rigid.velocity.magnitude;
			arrow.delayedDespawn = GameManager.GetSingleton<ObjectPool>().DelayDespawn (arrow.prefabIndex, arrow.gameObject, arrow.trs, arrowLifetime);
			CurrentArrowEntry.holdingCount --;
			CurrentArrowEntry.arrows = CurrentArrowEntry.arrows.Add(arrow);
			reloadTimer.Reset ();
			reloadTimer.Start ();
			GameManager.GetSingleton<AudioManager>().PlaySoundEffect (new SoundEffect.Settings(shootAudioClips[Random.Range(0, shootAudioClips.Length)]));
			if (GameManager.GetSingleton<GameManager>().movementJumpingShootingTutorialConversation.lastStartedDialog == GameManager.GetSingleton<GameManager>().shootingTutorialDialog)
				GameManager.GetSingleton<GameManager>().DeactivateGoForever (GameManager.GetSingleton<GameManager>().movementJumpingShootingTutorialConversation.gameObject);
		}

		public override void HandleVelocity ()
		{
			if (canMoveAndJump)
			{
				if (InputManager.UsingGamepad || isSwimming)
					Move (InputManager.SwimInput);
				else
					Move (Vector2.right * InputManager.MoveInput);
			}
			base.HandleVelocity ();
		}

		public override void Move (Vector2 move)
		{
			if (!isSwimming && move.x != 0)
				trs.localScale = trs.localScale.SetX(Mathf.Sign(move.x));
			base.Move (move);
			GameManager.GetSingleton<GameManager>().movementTutorialDialog.gameObject.SetActive(false);
		}

		public override void HandleJumping ()
		{
			if (!canMoveAndJump)
				return;
			if (InputManager.JumpInput && isGrounded && !isJumping)
				StartJump ();
			else if (isJumping)
			{
				if (!InputManager.JumpInput)
					StopJump ();
				if (rigid.velocity.y <= 0)
				{
					isJumping = false;
					GameManager.GetSingleton<GameManager>().jumpingTutorialDialog.gameObject.SetActive(false);
				}
			}
		}

		// public override void StartJump ()
		// {
		// 	base.StartJump ();
		// 	if (Physics2D.Linecast(bottomLeftColliderCorner + offsetPhysicsQueries, bottomRightColliderCorner + offsetPhysicsQueries, LayerMask.GetMask("Enemy")))
		// 		TakeDamage ();
		// }

		public override void HandleSwimming ()
		{
			base.HandleSwimming ();
			if (waterRectIAmIn != RectExtensions.NULL && IsUnderwater(waterRectIAmIn))
			{
				if (!isUnderwater)
				{
					anim.Play("Underwater");
					isUnderwater = true;
				}
				else if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Underwater"))
					TakeDamage ();
			}
			else
			{
				anim.Play("None");
				isUnderwater = false;
			}
		}

		public override void StopSwimming ()
		{
			base.StopSwimming ();
			isUnderwater = false;
		}

		public virtual void TakeDamage (float damage = 1)
		{
			if (isHurting)
				return;
			isHurting = true;
			GameManager.GetSingleton<GameManager>().screenEffectAnimator.Play("Hurt");
			hp --;
			Destroy(lifeIconsParent.GetChild(0).gameObject);
			if (hp <= 0)
				Death ();
		}

		public virtual void FullHeal ()
		{
			for (int i = (int) hp; i < maxHp; i ++)
				Instantiate(lifeIconsParent.GetChild(0), lifeIconsParent);
			hp = maxHp;
		}

		public virtual void Death ()
		{
			addToMoneyOnSave = 0;
			if (QuestManager.currentQuest != null)
				GameManager.onGameScenesLoaded += delegate { GameManager.GetSingleton<QuestManager>().ShowRetryScreen (); };
			// GameManager.GetSingleton<GameManager>().UnloadScene (GameManager.GetSingleton<GameManager>().gameScenes[0].name);
			GameManager.GetSingleton<GameManager>().LoadGameScenes ();
		}

		public virtual int GetArrowEntryIndex (Type arrowType)
		{
			for (int i = 0; i < arrowEntries.Length; i ++)
			{
				if (arrowType.Name == arrowEntries[i].arrowPrefab.GetType().Name)
					return i;
			}
			return -1;
		}

		public virtual ArrowEntry GetArrowEntry (Type arrowType)
		{
			return arrowEntries[GetArrowEntryIndex(arrowType)];
		}

		public virtual void OnTriggerExit2D (Collider2D other)
		{
			if (other.GetComponent<Arrow>() != null)
				arrowHasLeftBody = true;
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			if (arrowHasLeftBody && Time.time - timeOfLastShot >= Time.deltaTime * 2 && other.GetComponent<Arrow>() != null)
				Destroy (other.gameObject);
		}

		// public override void OnCollisionEnter2D (Collision2D coll)
		// {
		// 	base.OnCollisionEnter2D (coll);
		// 	Vector2[] normals = new Vector2[coll.contactCount];
		// 	Vector2 normal;
		// 	for (int i = 0; i < coll.contactCount; i ++)
		// 	{
		// 		normal = coll.GetContact(i).normal;
		// 		// foreach (KeyValuePair<Collider2D, Vector2[]> keyValuePair in collisionNormalsDict)
		// 		// {
		// 		// 	foreach (Vector2 collisionNormal in keyValuePair.Value)
		// 		// 	{
		// 		// 		if (Vector2.Angle(normal, collisionNormal) >= crushingAngle)
		// 		// 		{
		// 		// 			bool shouldDie = true;
		// 		// 			MoveTile moveTile = coll.gameObject.GetComponent<MoveTile>();
		// 		// 			if (moveTile != null)
		// 		// 				shouldDie = !moveTile.switchDirectionsWhenHit || Vector2.Angle((Vector2) moveTile.trs.position - moveTile.previousPosition, normal) >= crushingAngle;
		// 		// 			if (shouldDie)
		// 		// 			{
		// 		// 				Death ();
		// 		// 				return;
		// 		// 			}
		// 		// 		}
		// 		// 	}
		// 		// }
		// 		normals[i] = normal;
		// 	}
		// 	collisionNormalsDict.Add(coll.collider, normals);
		// }

		// public virtual void OnCollisionStay2D (Collision2D coll)
		// {
		// 	Vector2 normal;
		// 	Vector2[] normals = new Vector2[coll.contactCount];
		// 	for (int i = 0; i < coll.contactCount; i ++)
		// 	{
		// 		normal = coll.GetContact(i).normal;
		// 		foreach (KeyValuePair<Collider2D, Vector2[]> keyValuePair in collisionNormalsDict)
		// 		{
		// 			if (keyValuePair.Key != coll.collider)
		// 			{
		// 				foreach (Vector2 collisionNormal in keyValuePair.Value)
		// 				{
		// 					if (Vector2.Angle(normal, collisionNormal) >= crushingAngle)
		// 					{
		// 						bool shouldDie = true;
		// 						MoveTile moveTile = coll.gameObject.GetComponent<MoveTile>();
		// 						if (moveTile != null)
		// 							shouldDie = !moveTile.switchDirectionsWhenHit || Vector2.Angle((Vector2) moveTile.trs.position - moveTile.previousPosition, normal) >= crushingAngle;
		// 						if (shouldDie)
		// 						{
		// 							Death ();
		// 							return;
		// 						}
		// 					}
		// 				}
		// 			}
		// 		}
		// 		normals[i] = normal;
		// 	}
		// 	collisionNormalsDict[coll.collider] = normals;
		// }

		// public virtual void OnCollisionExit2D (Collision2D coll)
		// {
		// 	collisionNormalsDict.Remove(coll.collider);
		// }

		public virtual void AddMoney (int amount)
		{
			AccountManager.CurrentlyPlaying.CurrentMoney += amount;
			AccountManager.CurrentlyPlaying.TotalMoney += amount;
			moneyText.text = "" + AccountManager.CurrentlyPlaying.CurrentMoney;
			if (amount != 0)
			{
				moneyChangedAmountText.text = "" + AccountManager.CurrentlyPlaying.CurrentMoney;
				moneyChangedIndicator.Do ();
			}
		}

		[Serializable]
		public class ArrowEntry
		{
			public bool isOwned;
			public bool canShoot;
			public string keyToSwitchToMe;
			public GameObject menuOptionToSwitchToMe;
			public Arrow arrowPrefab;
			public int holdingCount;
			[HideInInspector]
			public Arrow[] arrows = new Arrow[0];
			public Image returnNotification;
		}
	}
}