using UnityEngine;
using System;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class AcidFog : MonoBehaviour
	{
		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			if (other == GameManager.GetSingleton<Player>().collider)
			{
				GameManager.GetSingleton<Player>().Death ();
			}
			else
			{
				Arrow arrow = other.GetComponent<Arrow>();
				if (arrow != null)
					arrow.gameObject.SetActive(false);
			}
		}
	}
}