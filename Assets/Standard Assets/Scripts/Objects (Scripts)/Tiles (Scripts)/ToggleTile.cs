using UnityEngine;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif

namespace FaceTracking
{
	[ExecuteInEditMode]
	public class ToggleTile : MonoBehaviour, ICopyable
	{
		public Transform trs;
		public SpriteRenderer spriteRenderer;
		public new Collider2D collider;
		public int chainDirection = 1;
		public ToggleTile[] correspondingToggleTiles = new ToggleTile[0];
		public int chainIndex;
		public LineRenderer lineRenderer;
		public float divideSpriteRendererAlphaOnDisable;
		public FloatRange lineRendererAlphaRange;
		public ToggleTile initActive;
		public ToggleTile currentActive;
		public bool isOn;
		public Timer undoTimer;
		public int numberOfTogglesAfterInit = 0;
		public float initSpriteRendererAlpha;
#if UNITY_EDITOR
		bool previousIsOn;
#endif

		public virtual void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				previousIsOn = isOn;
				EditorApplication.update += DoEditorUpdate;
				return;
			}
			else
				EditorApplication.update -= DoEditorUpdate;
#endif
			undoTimer.onFinished += Undo;
		}

#if UNITY_EDITOR
		// public virtual void OnEnable ()
		// {
		// 	if (!Application.isPlaying)
		// 	{
		// 		if (GetComponent<ObjectInWorld>().IsInPieces)
		// 			PrefabUtility.UnpackPrefabInstance(gameObject, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
		// 		return;
		// 	}
		// }

		public virtual void DoEditorUpdate ()
		{
			if (isOn != previousIsOn)
			{
				isOn = previousIsOn;
				TurnOnOrOff (!isOn);
			}
			chainIndex = correspondingToggleTiles.IndexOf(this);
			foreach (ToggleTile toggleTile in correspondingToggleTiles)
			{
				if (toggleTile.isOn)
				{
					initActive = toggleTile;
					break;
				}
			}
			if (initActive != null)
			{
				initActive.currentActive = initActive;
				foreach (ToggleTile toggleTile in correspondingToggleTiles)
				{
					toggleTile.correspondingToggleTiles = initActive.correspondingToggleTiles;
					toggleTile.lineRenderer.SetPosition(0, toggleTile.trs.position);
					toggleTile.UpdateLineRenderers ();
				}
			}
			previousIsOn = isOn;
		}
#endif

		public virtual void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				EditorApplication.update -= DoEditorUpdate;
				return;
			}
#endif
			undoTimer.onFinished -= Undo;
		}

		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			Toggle ();
		}

		// public virtual void OnCollisionStay2D (Collision2D coll)
		// {
		// 	Toggle ();
		// }

		public virtual void Toggle ()
		{
			if (chainIndex + chainDirection == correspondingToggleTiles.Length || chainIndex + chainDirection == -1)
				chainDirection *= -1;
			UpdateLineRenderers ();
			undoTimer.Stop ();
			ToggleTile nextToggleTile = correspondingToggleTiles[chainIndex + chainDirection];
			initActive.currentActive = nextToggleTile;
			GameManager.GetSingleton<Player>().SetIsGrounded ();
			if (GameManager.GetSingleton<Player>().isGrounded)
				GameManager.GetSingleton<Player>().velocityEffectors_Vector2Dict["Falling"].effect = Vector2.zero;
			GameManager.GetSingleton<Player>().HandleJumping ();
			nextToggleTile.TurnOnOrOff (!nextToggleTile.isOn);
			TurnOnOrOff (!isOn);
			nextToggleTile.undoTimer.Reset ();
			nextToggleTile.undoTimer.Start ();
			initActive.numberOfTogglesAfterInit ++;
		}

		public virtual void Undo (params object[] args)
		{
			if (initActive.numberOfTogglesAfterInit == 0)
				return;
			if (chainIndex - chainDirection == correspondingToggleTiles.Length || chainIndex - chainDirection == -1)
				chainDirection *= -1;
			UpdateLineRenderers ();
			TurnOnOrOff (!isOn);
			ToggleTile nextToggleTile = correspondingToggleTiles[chainIndex - chainDirection];
			initActive.currentActive = nextToggleTile;
			nextToggleTile.TurnOnOrOff (!nextToggleTile.isOn);
			initActive.numberOfTogglesAfterInit --;
			if (initActive.numberOfTogglesAfterInit > 0)
			{
				nextToggleTile.undoTimer.Reset ();
				nextToggleTile.undoTimer.Start ();
			}
		}

		public virtual void TurnOnOrOff (bool turnOn)
		{
			if (!isOn && turnOn)
				spriteRenderer.color = spriteRenderer.color.SetAlpha(initSpriteRendererAlpha);
			else if (isOn && !turnOn)
				spriteRenderer.color = spriteRenderer.color.DivideAlpha(divideSpriteRendererAlphaOnDisable);
			collider.enabled = turnOn;
			isOn = turnOn;
#if UNITY_EDITOR
			previousIsOn = turnOn;
#endif
		}

		public virtual void UpdateLineRenderers ()
		{
			LineRenderer _lineRenderer;
			ToggleTile toggleTile;
			if (chainDirection > 0)
			{
				for (int i = 0; i < correspondingToggleTiles.Length; i ++)
				{
					toggleTile = correspondingToggleTiles[i];
					_lineRenderer = toggleTile.lineRenderer;
					_lineRenderer.startColor = _lineRenderer.startColor.SetAlpha(Mathf.Lerp(lineRendererAlphaRange.min, lineRendererAlphaRange.max, correspondingToggleTiles.Length - (float) (i + 1) / correspondingToggleTiles.Length));
					_lineRenderer.endColor = _lineRenderer.endColor.SetAlpha(Mathf.Lerp(lineRendererAlphaRange.min, lineRendererAlphaRange.max, correspondingToggleTiles.Length - (float) (i + 1) / correspondingToggleTiles.Length));
					if (i < correspondingToggleTiles.Length - 1 && i >= chainIndex)
					{
						_lineRenderer.SetPosition(1, correspondingToggleTiles[i + 1].trs.position);
						_lineRenderer.enabled = true;
					}
					else
						_lineRenderer.enabled = false;
				}
			}
			else
			{
				for (int i = correspondingToggleTiles.Length - 1; i >= 0; i --)
				{
					toggleTile = correspondingToggleTiles[i];
					_lineRenderer = toggleTile.lineRenderer;
					_lineRenderer.startColor = _lineRenderer.startColor.SetAlpha(Mathf.Lerp(lineRendererAlphaRange.min, lineRendererAlphaRange.max, (float) (i + 1) / correspondingToggleTiles.Length));
					_lineRenderer.endColor = _lineRenderer.endColor.SetAlpha(Mathf.Lerp(lineRendererAlphaRange.min, lineRendererAlphaRange.max, (float) (i + 1) / correspondingToggleTiles.Length));
					if (i > 0 && i <= chainIndex)
					{
						_lineRenderer.SetPosition(1, correspondingToggleTiles[i - 1].trs.position);
						_lineRenderer.enabled = true;
					}
					else
						_lineRenderer.enabled = false;
				}
			}
		}

		public virtual void Copy (object copy)
		{
#if UNITY_EDITOR
			EditorCoroutineUtility.StartCoroutine(CopyRoutine (copy as ToggleTile), this);
#endif
		}

#if UNITY_EDITOR
		public virtual IEnumerator CopyRoutine (ToggleTile toggleTile)
		{
			EditorApplication.update -= DoEditorUpdate;
			trs = GetComponent<Transform>().GetChild(0);
			lineRenderer = GetComponent<LineRenderer>();
			spriteRenderer = GetComponentInChildren<SpriteRenderer>();
			collider = GetComponentInChildren<Collider2D>();
			TurnOnOrOff (toggleTile.isOn);
			divideSpriteRendererAlphaOnDisable = toggleTile.divideSpriteRendererAlphaOnDisable;
			lineRendererAlphaRange = toggleTile.lineRendererAlphaRange;
			chainDirection = toggleTile.chainDirection;
			correspondingToggleTiles = new ToggleTile[toggleTile.correspondingToggleTiles.Length];
			ObjectInWorld worldObject = GetComponent<ObjectInWorld>();
			ObjectInWorld otherWorldObject;
			for (int i = 0; i < correspondingToggleTiles.Length; i ++)
			{
				otherWorldObject = toggleTile.correspondingToggleTiles[i].GetComponent<ObjectInWorld>();
				if (worldObject.IsInPieces != otherWorldObject.IsInPieces)
				{
					yield return new WaitUntil(() => (otherWorldObject.duplicateGo != null));
					correspondingToggleTiles[i] = otherWorldObject.duplicateGo.GetComponent<ToggleTile>();
				}
			}
			DoEditorUpdate ();
		}
#endif
	}
}