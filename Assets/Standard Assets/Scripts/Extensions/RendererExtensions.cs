using UnityEngine;
using FaceTracking;

namespace Extensions
{
	public static class RendererExtensions
	{
		public static Color GetColor (this SpriteRenderer spriteRenderer, Vector2 worldPosition, Transform trs)
		{
			Rect rect = spriteRenderer.GetRect(trs);
			if (!rect.Contains(worldPosition))
				return ColorExtensions.NULL;
			else
			{
				Texture2D texture = spriteRenderer.sprite.texture;
				Vector2Int textureSize = new Vector2Int(texture.width, texture.height);
				Vector2 normalizedPosition = Rect.PointToNormalized(rect, worldPosition);
				normalizedPosition = normalizedPosition.Rotate(spriteRenderer.sprite.pivot, -trs.eulerAngles.z);
				Vector2Int pixel = textureSize.Multiply(normalizedPosition);
				return texture.GetPixel(pixel.x, pixel.y);
			}
		}
		
		public static Vector2Int GetTexturePixelPosition (this SpriteRenderer spriteRenderer, Vector2 worldPosition, Transform trs)
		{
			Rect rect = spriteRenderer.GetRect(trs);
			if (!rect.Contains(worldPosition))
				return VectorExtensions.NULL2INT;
			else
			{
				Texture2D texture = spriteRenderer.sprite.texture;
				Vector2Int textureSize = new Vector2Int(texture.width, texture.height);
				Vector2 normalizedPosition = Rect.PointToNormalized(rect, worldPosition);
				normalizedPosition = normalizedPosition.Rotate(spriteRenderer.sprite.pivot, -trs.eulerAngles.z);
				return textureSize.Multiply(normalizedPosition);
			}
		}

		// TODO: Take into account sprite pivot in edit mode
		// TODO: Take into account rotation in edit mode
		public static Rect GetRect (this SpriteRenderer spriteRenderer, Transform trs)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				Vector2Int textureSize = spriteRenderer.GetTextureSize();
				Rect output = Rect.MinMaxRect(-textureSize.x / 2, -textureSize.y / 2, textureSize.x / 2, textureSize.y / 2);
				output.center += (Vector2) trs.position;
				output.size = output.size.Multiply(trs.lossyScale);
				return output;
			}
#endif
			return spriteRenderer.bounds.ToRect();
		}

		public static Vector2Int GetTextureSize (this SpriteRenderer spriteRenderer)
		{
			Texture2D texture = spriteRenderer.sprite.texture;
			return new Vector2Int(texture.width, texture.height);
		}
	}
}