using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace FaceTracking
{
	//[ExecuteInEditMode]
	public class AccountManager : MonoBehaviour
	{
		public static Account[] Accounts
		{
			get
			{
				List<Account> output = new List<Account>();
				foreach (AccountSelectMenuOption accountSelectMenuOption in GameManager.GetSingleton<AccountSelectMenu>().menuOptions)
					output.Add(accountSelectMenuOption.account);
				return output.ToArray();
			}
		}
		public static Account CurrentlyPlaying
		{
			get
			{
				if (lastUsedAccountIndex == -1)
					return null;
				return Accounts[lastUsedAccountIndex];
			}
		}
		public static int lastUsedAccountIndex = -1;
	}
}
