﻿using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;
using UnityEngine.InputSystem;

namespace FaceTracking
{
	public class InputManager : MonoBehaviour
	{
		public InputSettings settings;
		public static InputSettings Settings
		{
			get
			{
				return GameManager.GetSingleton<InputManager>().settings;
			}
		}
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static float MoveInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.leftStick.x.ReadValue();
				else
				{
					int output = 0;
					if (Keyboard.current.dKey.isPressed)
						output ++;
					if (Keyboard.current.aKey.isPressed)
						output --;
					return output;
				}
			}
		}
		public float _MoveInput
		{
			get
			{
				return MoveInput;
			}
		}
		public static Vector2 SwimInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ToVec2(), 1);
				else
				{
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(MoveInput, y), 1);
				}
			}
		}
		public Vector2 _SwimInput
		{
			get
			{
				return SwimInput;
			}
		}
		public static bool JumpInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.leftTrigger.isPressed;
				else
					return Keyboard.current.wKey.isPressed;
			}
		}
		public bool _JumpInput
		{
			get
			{
				return JumpInput;
			}
		}
		public static bool InteractInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else
					return Keyboard.current.eKey.isPressed;
			}
		}
		public bool _InteractInput
		{
			get
			{
				return InteractInput;
			}
		}
		public static float ZoomInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightStick.y.ReadValue();
				else
					return Mouse.current.scroll.y.ReadValue();
			}
		}
		public float _ZoomInput
		{
			get
			{
				return ZoomInput;
			}
		}
		public static bool SubmitInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else
					return Keyboard.current.enterKey.isPressed;// || Mouse.current.leftButton.isPressed;
			}
		}
		public bool _SubmitInput
		{
			get
			{
				return SubmitInput;
			}
		}
		public static bool ArrowActionInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightTrigger.isPressed;
				else
					return Mouse.current.leftButton.isPressed;
			}
		}
		public bool _ArrowActionInput
		{
			get
			{
				return ArrowActionInput;
			}
		}
		public static int SwitchArrowInput
		{
			get
			{
				if (UsingGamepad)
				{
					return -1;
				}
				else
				{
					if (Keyboard.current.spaceKey.isPressed)
						return 0;
					else if (Keyboard.current.numpad1Key.isPressed)
						return 1;
					else if (Keyboard.current.numpad2Key.isPressed)
						return 2;
					else if (Keyboard.current.numpad3Key.isPressed)
						return 3;
					else if (Keyboard.current.numpad4Key.isPressed)
						return 4;
					return -1;
				}
			}
		}
		public int _SwitchArrowInput
		{
			get
			{
				return SwitchArrowInput;
			}
		}
		public static bool ShootInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightTrigger.isPressed || Gamepad.current.rightStickButton.isPressed;
				else
					return Mouse.current.leftButton.isPressed;
			}
		}
		public bool _ShootInput
		{
			get
			{
				return ShootInput;
			}
		}
		public static bool ArrowMenuInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightShoulder.isPressed || Gamepad.current.leftShoulder.isPressed;
				else
					return Mouse.current.rightButton.isPressed;
			}
		}
		public bool _ArrowMenuInput
		{
			get
			{
				return ArrowMenuInput;
			}
		}
		public static int SwitchMenuSectionInput
		{
			get
			{
				if (UsingGamepad)
				{
					int output = 0;
					if (Gamepad.current.rightShoulder.isPressed)
						output ++;
					if (Gamepad.current.leftShoulder.isPressed)
						output --;
					return output;
				}
				else
					return 0;
			}
		}
		public int _SwitchMenuSectionInput
		{
			get
			{
				return SwitchMenuSectionInput;
			}
		}
		public static Vector2 AimInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.rightStick.ToVec2(), 1);
				else
					return Vector2.ClampMagnitude(GameManager.GetSingleton<GameCamera>().camera.ScreenToWorldPoint(MousePosition) - GameManager.GetSingleton<Player>().trs.position, 1);
			}
		}
		public Vector2 _AimInput
		{
			get
			{
				return AimInput;
			}
		}
		public static Vector2 UIMovementInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ToVec2(), 1);
				else
				{
					int x = 0;
					if (Keyboard.current.dKey.isPressed)
						x ++;
					if (Keyboard.current.aKey.isPressed)
						x --;
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(x, y), 1);
				}
			}
		}
		public Vector2 _UIMovementInput
		{
			get
			{
				return UIMovementInput;
			}
		}
		public static bool PauseInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.startButton.isPressed || Gamepad.current.selectButton.isPressed;
				else
					return Keyboard.current.escapeKey.isPressed;
			}
		}
		public bool _PauseInput
		{
			get
			{
				return PauseInput;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				if (UsingGamepad)
					return false;
				else
					return Mouse.current.leftButton.isPressed;
			}
		}
		public bool _LeftClickInput
		{
			get
			{
				return LeftClickInput;
			}
		}
		public static Vector2 MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ToVec2();
				else
					return GameManager.activeCursorEntry.rectTrs.position;
			}
		}
		public Vector2 _MousePosition
		{
			get
			{
				return MousePosition;
			}
		}
		public static bool ClearDataInput
		{
			get
			{
				if (UsingKeyboard)
					return Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.leftShiftKey.isPressed && Keyboard.current.cKey.isPressed && Keyboard.current.dKey.isPressed;
				else
					return false;
			}
		}
		public bool _ClearDataInput
		{
			get
			{
				return ClearDataInput;
			}
		}
		
		public virtual void Start ()
		{
			InputSystem.onDeviceChange += OnDeviceChanged;
		}
		
		public virtual void OnDeviceChanged (InputDevice device, InputDeviceChange change)
		{
			if (device is Gamepad)
			{
				if (change == InputDeviceChange.Added || change == InputDeviceChange.Reconnected)
				{
					GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(false);
					if (GameManager.GetSingleton<VirtualKeyboard>() != null)
						GameManager.GetSingleton<VirtualKeyboard>().outputToInputField.readOnly = true;
				}
				else if (change == InputDeviceChange.Removed || change == InputDeviceChange.Disconnected)
				{
					GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(true);
					if (GameManager.GetSingleton<VirtualKeyboard>() != null)
						GameManager.GetSingleton<VirtualKeyboard>().outputToInputField.readOnly = false;
				}
				foreach (_Text text in _Text.instances)
					text.UpdateText ();
			}
		}
		
		public virtual void OnDestroy ()
		{
			InputSystem.onDeviceChange -= OnDeviceChanged;
		}

		public static Vector2 GetWorldMousePosition ()
		{
			Rect gameViewRect = GameManager.GetSingleton<GameManager>().gameViewRectTrs.GetWorldRect();
			return GameManager.GetSingleton<CameraScript>().camera.ViewportToWorldPoint(gameViewRect.ToNormalizedPosition(MousePosition));
		}
	}

	// [Serializable]
	// public class InputButton
	// {
	// 	public string[] buttonNames;
	// 	public KeyCode[] keyCodes;

	// 	public virtual bool GetDown ()
	// 	{
	// 		bool output = false;
	// 		foreach (KeyCode keyCode in keyCodes)
	// 			output |= Input.GetKeyDown(keyCode);
	// 		foreach (string buttonName in buttonNames)
	// 			output |= InputManager.inputter.GetButtonDown(buttonName);
	// 		return output;
	// 	}

	// 	public virtual bool Get ()
	// 	{
	// 		bool output = false;
	// 		foreach (KeyCode keyCode in keyCodes)
	// 			output |= Input.GetKey(keyCode);
	// 		foreach (string buttonName in buttonNames)
	// 			output |= InputManager.inputter.GetButton(buttonName);
	// 		return output;
	// 	}

	// 	public virtual bool GetUp ()
	// 	{
	// 		bool output = false;
	// 		foreach (KeyCode keyCode in keyCodes)
	// 			output |= Input.GetKeyUp(keyCode);
	// 		foreach (string buttonName in buttonNames)
	// 			output |= InputManager.inputter.GetButtonUp(buttonName);
	// 		return output;
	// 	}
	// }

	// [Serializable]
	// public class InputAxis
	// {
	// 	public InputButton positiveButton;
	// 	public InputButton negativeButton;

	// 	public virtual int Get ()
	// 	{
	// 		int output = 0;
	// 		if (positiveButton.Get())
	// 			output ++;
	// 		if (negativeButton.Get())
	// 			output --;
	// 		return output;
	// 	}
	// }

	// public enum InputDevice
	// {
	// 	Keyboard,
	// 	Gamepad
	// }
}